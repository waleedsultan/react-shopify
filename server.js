const express = require("express");
const path = require('path');
const app = express();
const dotenv = require('dotenv');

dotenv.config();

app.use(express.json());
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, 'build')));

app.get('/ping', function (req, res) {
    return res.send('pong');
});

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

const server = require('http').createServer(app);
server.setTimeout(3600000); 

try {
    server.listen(process.env.PORT || '3000', function () {
        console.log('Server turned on with', process.env.APP_ENV, 'mode on port', process.env.PORT);
    });
} catch (ex) {
    console.log(ex);
} 